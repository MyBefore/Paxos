package com.cat.paxos.proposer;

import com.cat.paxos.acceptor.ConfirmProposalResponse;
import com.cat.paxos.acceptor.ReceiveProposalResponse;

import java.util.List;

/**
 * 接收者的策略
 * @auther Cat.wang
 * @date 2020/1/15 17:49
 */
public interface AcceptorStrategy {

    /**
     * 决定提交结果
     * @param total
     * @param responses
     * @return
     */
    CommitWaitConfirmProposalResponse decisionCommitWaitConfirmProposal(int total, List<ReceiveProposalResponse> responses) throws CommitProposalException;

    /**
     * 决定提交确认结果
     * @param total
     * @param responses
     * @return
     */
    CommitProposalResponse decisionCommitConfirmProposal(int total, List<ConfirmProposalResponse> responses) throws CommitProposalException;

    /**
     * 决定提交编号
     * @param total
     * @param proposalNumbers
     * @return
     */
    String decisionProposalNumber(int total, List<String> proposalNumbers) throws PrepareProposalException;

}
