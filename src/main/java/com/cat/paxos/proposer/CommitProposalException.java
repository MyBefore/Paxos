package com.cat.paxos.proposer;

import com.cat.paxos.exception.ProposalException;

/**
 * 提交提案异常
 * @auther Cat.wang
 * @date 2020/1/15 16:44
 */
public class CommitProposalException extends ProposalException {
    public CommitProposalException() {
    }

    public CommitProposalException(String message) {
        super(message);
    }

    public CommitProposalException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommitProposalException(Throwable cause) {
        super(cause);
    }

    public CommitProposalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
