package com.cat.paxos.proposer;

import com.cat.paxos.acceptor.Acceptor;
import com.cat.paxos.proposal.Proposal;

import java.util.List;

/**
 * 提案者
 * @auther Cat.wang
 * @date 2020/1/15 15:18
 */
public interface Proposer {

    /**
     * 获取数据的提案
     * @param payload
     * @param <T>
     * @return
     */
    <T> Proposal<T> prepare(T payload) throws PrepareProposalException;

    /**
     * 提交提案
     * @param proposal
     * @return
     */
    <T> CommitProposalResponse commit(Proposal<T> proposal) throws CommitProposalException;

    /**
     * 接收者集合
     * @return
     */
    List<Acceptor> acceptors();

    /**
     * 接收者策略
     * @return
     */
    AcceptorStrategy strategy();

}
