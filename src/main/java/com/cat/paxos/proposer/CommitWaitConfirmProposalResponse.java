package com.cat.paxos.proposer;

import com.cat.paxos.common.Response;

/**
 * 提交待确认提案回应
 * @auther Cat.wang
 * @date 2020/1/16 15:41
 */
public interface CommitWaitConfirmProposalResponse extends Response {

    /**
     * 获取提案编号 成功返回当前的提案编号，失败返回接收的最大提案编号
     * @return
     */
    String proposalNumber();

}
