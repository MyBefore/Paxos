package com.cat.paxos.proposer;

import com.cat.paxos.exception.ProposalException;

/**
 * 准备提案异常
 * @auther Cat.wang
 * @date 2020/1/15 16:20
 */
public class PrepareProposalException extends ProposalException {
    public PrepareProposalException() {
    }

    public PrepareProposalException(String message) {
        super(message);
    }

    public PrepareProposalException(String message, Throwable cause) {
        super(message, cause);
    }

    public PrepareProposalException(Throwable cause) {
        super(cause);
    }

    public PrepareProposalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
