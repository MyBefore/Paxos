package com.cat.paxos.proposal;

import java.io.Serializable;

/**
 * @auther Cat.wang
 * @date 2020/1/15 15:40
 */
public interface PayloadSerializer<T> {

    /**
     * 序列化
     * @param data
     * @param <D>
     * @return
     */
    <D extends Serializable> D encode(T data);

    /**
     * 反序列化
     * @param data
     * @param <D>
     * @return
     */
    <D extends Serializable> T decode(D data);

}
