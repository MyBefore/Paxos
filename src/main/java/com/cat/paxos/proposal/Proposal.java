package com.cat.paxos.proposal;

/**
 * 提案
 * @auther Cat.wang
 * @date 2020/1/15 15:21
 */
public interface Proposal<T> {

    /**
     * 提案编号
     * @return
     */
    String number();

    /**
     * 获取负载的数据
     * @return
     */
    T payload();

    /**
     * 获取负载序列化器
     * @return
     */
    PayloadSerializer<T> serializer();

}
