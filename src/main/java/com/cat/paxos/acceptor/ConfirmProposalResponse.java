package com.cat.paxos.acceptor;

import com.cat.paxos.common.Response;

/**
 * 确认提案回应
 * @auther Cat.wang
 * @date 2020/1/15 15:50
 */
public interface ConfirmProposalResponse extends Response {

    /**
     * 获取提案编号 成功返回当前的提案编号，失败返回接收的最大提案编号
     * @return
     */
    String proposalNumber();

}
