package com.cat.paxos.acceptor;

import com.cat.paxos.learner.LeanerStrategy;
import com.cat.paxos.learner.Learner;
import com.cat.paxos.proposal.Proposal;

import java.util.List;

/**
 * 接收者
 * @auther Cat.wang
 * @date 2020/1/15 15:17
 */
public interface Acceptor {

    /**
     * 获取提案编号
     * @return
     */
    String getProposalNumber();

    /**
     * 接收提案
     * @param proposal
     * @return
     */
    <T> ReceiveProposalResponse receive(Proposal<T> proposal);

    /**
     * 确认提案
     * @param proposalNumber 提案编号
     * @return
     */
    ConfirmProposalResponse confirm(String proposalNumber);

    /**
     * 学习者集合
     * @return
     */
    List<Learner> learners();

    /**
     * 学习者策略
     * @return
     */
    LeanerStrategy strategy();

}
