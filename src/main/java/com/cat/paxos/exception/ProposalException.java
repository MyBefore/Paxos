package com.cat.paxos.exception;

/**
 * 提案异常
 * @auther Cat.wang
 * @date 2020/1/15 16:45
 */
public class ProposalException extends Exception {
    public ProposalException() {
    }

    public ProposalException(String message) {
        super(message);
    }

    public ProposalException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProposalException(Throwable cause) {
        super(cause);
    }

    public ProposalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
