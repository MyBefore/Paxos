package com.cat.paxos.learner;

import com.cat.paxos.acceptor.ConfirmProposalResponse;

import java.util.List;

/**
 * 学习者策略
 * @auther Cat.wang
 * @date 2020/1/16 16:52
 */
public interface LeanerStrategy {

    /**
     * 决定提案编号是否是可以使用的
     * @param maxProposalNumber
     * @param propsalNumber
     * @return
     */
    boolean decisionProposalNumberOk(String maxProposalNumber, String propsalNumber);

    /**
     * 决定学习提案结果
     * @param total
     * @param responses
     * @return
     */
    ConfirmProposalResponse decisionLeanProposal(int total, List<LearnProposalResponse> responses);

}
