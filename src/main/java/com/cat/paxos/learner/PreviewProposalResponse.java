package com.cat.paxos.learner;

import com.cat.paxos.common.Response;

import java.util.List;

/**
 * 预习提案
 * @auther Cat.wang
 * @date 2020/1/16 17:53
 */
public interface PreviewProposalResponse extends Response {

    /**
     * 获取提案编号 成功返回当前的提案编号，失败返回接收的最大提案编号
     * @return
     */
    String proposalNumber();

    /**
     * 已学习过
     * @return
     */
    boolean learned();

    /**
     * 需要学习的学习者
     * @return
     */
    List<Learner> needLearns();

}
