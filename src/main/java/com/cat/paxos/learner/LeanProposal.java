package com.cat.paxos.learner;

import com.cat.paxos.proposal.Proposal;

/**
 * @auther Cat.wang
 * @date 2020/1/17 17:10
 */
public interface LeanProposal {

    /**
     * 提案编号
     * @return
     */
    String proposalNumber();

    /**
     * 学习的提案
     * @return
     */
    Proposal proposal();

    /**
     * 设置学习的提案
     * @param proposal
     */
    void proposal(Proposal proposal);
}
