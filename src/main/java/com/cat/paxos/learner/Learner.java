package com.cat.paxos.learner;

import com.cat.paxos.proposal.Proposal;

import java.util.List;

/**
 * 学习者
 * @auther Cat.wang
 * @date 2020/1/15 15:18
 */
public interface Learner {

    /**
     * 预习提案
     * @param proposalNumber
     * @return
     */
    PreviewProposalResponse preview(String proposalNumber);

    /**
     * 学习提案
     * @param proposal
     * @param <T>
     * @return
     */
    <T> LearnProposalResponse learn(Proposal<T> proposal);

    /**
     * 其它学习者
     * @return
     */
    List<Learner> others();

}
