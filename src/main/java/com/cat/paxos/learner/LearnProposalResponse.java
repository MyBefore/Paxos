package com.cat.paxos.learner;

import com.cat.paxos.common.Response;

/**
 * 学习天的回应
 * @auther Cat.wang
 * @date 2020/1/16 16:37
 */
public interface LearnProposalResponse extends Response {

    /**
     * 获取提案编号 成功返回当前的提案编号，失败返回接收的最大提案编号
     * @return
     */
    String proposalNumber();

}
